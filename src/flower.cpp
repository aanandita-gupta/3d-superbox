#include "flower.h"

Flower::Flower(float x, float y, float z, float w, float h, float d)
	: SuperBox(x, y, z, w, h, d)
{
	middle.setRadius(w * 0.15);
	middle.setPosition(0, 0, 0);

	stalk.set(w * 0.04, h* 0.7);
	stalk.setPosition(0, stalk.getHeight()/2, -middle.getRadius());


	numOfPetals = 10;
	for (int i = 0; i < numOfPetals; i++) {
		ofConePrimitive p;
		p.set(w * 0.1, w * 0.4);
		p.setPosition(0, (-middle.getRadius())-(petal.getHeight()/6.7), 0);
		petals.push_back(p);
	}
}

Flower::~Flower()
{
}

void Flower::draw()
{
	ofPushMatrix();
		ofTranslate(getXPos(), getYPos(), getZPos());
	
		ofPushStyle();
		ofSetColor(0, 255, 0);
		ofFill();
		stalk.draw();
		ofPopStyle();

		ofPushMatrix();
		ofRotateDeg(10, 0, 0, 1);
			float petalDeg = 360.f / (float)petals.size();
			for (float i = 0; i < petals.size(); i++) {
				ofRotateDeg(petalDeg, 0, 0, 1);
				//cout << petalDeg * i << '\n';
				petals[i].draw();
			}
		ofPopMatrix();

		ofRotateDeg(90, 0, 1, 0);
		middleTexture.bind();
		middle.draw();
		middleTexture.unbind();

	ofPopMatrix();

}
