//https://learn.gold.ac.uk/mod/forum/discuss.php?d=239665#p348727 - Thomas Noya's forum post

//  RockBlock.hpp
//  Lab06_Pointers
//
//  Created by Thomas Noya TNOYA001 on 12/10/19.
//

#ifndef RockBlock_hpp
#define RockBlock_hpp

#include <stdio.h>
#include "superBox.h"
#include "ofMain.h"

class RockBlock : public SuperBox {
public:
    RockBlock(float x, float y, float z, float w, float h, float d);
    virtual void draw();
    void setTexture(ofImage& _texture);
private:
    float xPos;
    float yPos;
    float zPos;
    ofBoxPrimitive box;
    ofImage texture;
};

#endif /* RockBlock_hpp */
