//https://learn.gold.ac.uk/mod/forum/discuss.php?d=240055#p349282 - Esther Downton's forum post

#pragma once
#ifndef FLOWER_H
#define FLOWER_H

#include "superBox.h"
#include "ofMain.h"
#include <vector>

class Flower : public SuperBox
{
public:
	Flower(float x, float y, float z, float w, float h, float d);
	~Flower();
	void draw();

private:
	ofSpherePrimitive middle;
	ofImage middleTexture{ "daisy_center.jpg" };
	ofConePrimitive petal;
	ofCylinderPrimitive stalk;
	int numOfPetals;
	std::vector <ofConePrimitive> petals;
};

#endif // FLOWER_H

