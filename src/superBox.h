//
//  superBox.h
//  World
//
//  Created by Aanandita Gupta on 06/12/2019.
//

#ifndef superBox_h
#define superBox_h

#include <stdio.h>



#include "ofMain.h"


class SuperBox
{
public:
    SuperBox(float x, float y, float z, float w, float h, float d);
    //~SuperBox();
    virtual void draw();
    void setTexture(ofImage& _texture);
    float getXPos();
    float getYPos();
    float getZPos();
private:
    float xPos;
    float yPos;
    float zPos;
    ofImage texture;
    ofBoxPrimitive box;
};

#endif /* superBox_hpp */
