//
//  positionmarker.h
//  World
//
//  Created by Aanandita Gupta on 06/12/2019.
//

#ifndef positionmarker_h
#define positionmarker_h

#include <stdio.h>

#include <vector>

#include "ofMain.h"
#include "superBox.h"

class PositionMarker
{
public:
    PositionMarker(float blockSize);
    void setMousePosition(float mouseX, float mouseY, float w, float h);
    void draw();
    float getXPos();
    float getYPos();
    float getZPos();
    void moveAboveBlocks(std::vector <SuperBox*>& blocks);
private:
    ofMaterial material;
    ofBoxPrimitive box;
    float xPos;
    float yPos;
    float zPos;
    float blockSize;
};


#endif /* positionmarker_hpp */
