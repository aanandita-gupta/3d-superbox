//
//  superBox.cpp
//  World
//
//  Created by Aanandita Gupta on 06/12/2019.
//

#include "superBox.h"

SuperBox::SuperBox(float x, float y, float z, float w, float h, float d)
{
    xPos = x;
    yPos = y;
    zPos = z;
    box.setWidth(w);
    box.setHeight(h);
    box.setDepth(d);
}

void SuperBox::draw()
{
    ofPushMatrix();
    texture.bind();
        ofTranslate(xPos, yPos, zPos);
        box.draw();
    texture.unbind();
    ofPopMatrix();
}

void SuperBox::setTexture(ofImage& _texture)
{
    texture = _texture;
}


float SuperBox::getXPos()
{
    return xPos;
}
float SuperBox::getYPos()
{
    return yPos;
}
float SuperBox::getZPos()
{
    return zPos;
}

